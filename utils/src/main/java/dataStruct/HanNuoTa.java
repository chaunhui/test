package dataStruct;

import java.util.Scanner;

/**
 * @author: 向传辉
 * @createTime: 2022/2/11 17:04
 */
public class HanNuoTa {
    static void queen(int n) {
        int i = 1;
        int[] a = new int[n + 2];
        a[i] = 0;
        while (i > 0) {
            ++a[i];
            for (; a[i] <= n; a[i]++) {
                if (check(i, a)) {
                    break;
                }
            }
            if (a[i] <= n) {
                if (i == n) {
                    for (int k = 1; k <= n; k++) {
                        System.out.print("(" + k + "," + a[k] + ")  ");
                    }
                    System.out.println();
                } else {
                    i++;
                    a[i] = 0;
                }
            } else {
                i--;
            }
        }
    }

    static boolean check(int j, int[] a) {
        for (int i = 1; i < j; i++) {
            if (a[i] == a[j] || (Math.abs(a[i] - a[j]) == Math.abs(i - j))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.println("n皇后问题，皇后数量：（0-20）");
        n = sc.nextInt();
        queen(n);
    }
}
