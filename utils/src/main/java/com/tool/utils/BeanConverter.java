package com.tool.utils;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

// 对象字段复制
public class BeanConverter {
    /**
     * 根据对象以及一个类的class进行对象参数复制
     *
     * @param bean  被复制的对象
     * @param clazz 需要创建的对象class
     * @param <T>
     * @return 返回被创建的对象
     */
    public static <T> T convertBean(Object bean, Class<T> clazz) {
        try {
            T result = clazz.newInstance();
            BeanUtils.copyProperties1(bean, result);
            return result;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * 复制一个集合的对象，
     *
     * @param list  被复制的集合对象
     * @param clazz 需要创建的对象class
     * @param <T>
     * @return 返回一个创建的集合对象
     */
    public static <T> List<T> convertList(List<?> list, Class<? extends T> clazz) {
        List<T> result = new ArrayList<>(list.size());
        try {
            for (Object obj : list) {
                T t = clazz.newInstance();
                BeanUtils.copyProperties1(obj, t);
                result.add(t);
            }
            return result;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static String convertToXml(Object obj) {
        return convertToXml(obj, "UTF-8");
    }

    /**
     * 将java Bean对象 转换为 Xml
     *
     * @param obj      java对象
     * @param encoding 字符集
     * @return 一个xml的字符串
     */
    public static String convertToXml(Object obj, String encoding) {
        String result = null;
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
            StringWriter writer = new StringWriter();
            marshaller.marshal(obj, writer);
            result = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 将xml文件转换为javaBean对象
     *
     * @param xml   xml字符串
     * @param clazz 需要转换成的对象class
     * @param <T>
     * @return 转换后的对象
     */
    public static <T> T convertToJavaBean(String xml, Class<T> clazz) {
        T t = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            //防止XXE攻击
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dbf.setXIncludeAware(false);
            dbf.setExpandEntityReferences(false);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new InputSource(new StringReader(xml.replaceAll("&(?!\\w+;)", "&amp;"))));

            t = (T) unmarshaller.unmarshal(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }
}
