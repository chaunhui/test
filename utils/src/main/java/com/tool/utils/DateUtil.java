package com.tool.utils;

import org.apache.dubbo.common.utils.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 日期工具类：SimpleDateFormat非线程同步类，因此每次创建
 *
 * @author: Hui
 * @createTime:2022/1/28 8:48
 */
public class  DateUtil {
    /**
     * 日期格式yyyy-MM-dd
     */
    private static final String DATE_FORMAT_STRING_SEPARATE = "yyyy-MM-dd";
    private static final String DATE_FORMAT_MASK_SEPARATE = "^\\d{4}-\\d{2}-\\d{2}$";
    /**
     * 日期格式：yyyyMMdd
     */
    private static final String DATE_FORMAT_STRING = "yyyyMMdd";
    private static final String DATE_FORMAT_DATE_MASK = "^\\d{8}$";

    // yyyy-MM-dd 正则表达式，包括平年闰年的判断
    public static final String DATE_PATTERN_STRING = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-" +
            "(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|" +
            "((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)";
    /**
     * 时间格式：HHmmss
     */
    private static final String DATE_FORMAT_TIME = "HHmmss";
    private static final String DATE_FORMAT_TIME_MASK = "^\\d{6}$";
    /**
     * 时间格式：HH:mm:ss
     */
    private static final String TIME_FORMAT = "HH:mm:ss";
    private static final String TIME_FORMAT_MASK = "^\\d{2}:\\d{2}:\\d{2}";
    /**
     * 日期格式：yyyyMMddHHmmss
     */
    private static final String TIMESTAMP_FORMAT_NO_SEPARATE = "yyyyMMddHHmmss";
    private static final String TIMESTAMP_FORMAT_NO_SEPARATE_MASK = "^\\d{14}$";
    /**
     * 注册的日期掩码
     */
    static Map<String, String> PATTERNS = new HashMap<>();

    static {
        PATTERNS.put(DATE_FORMAT_DATE_MASK, DATE_FORMAT_STRING);
        PATTERNS.put(DATE_FORMAT_MASK_SEPARATE, DATE_FORMAT_STRING_SEPARATE);
        PATTERNS.put(DATE_FORMAT_TIME_MASK, DATE_FORMAT_TIME);
        PATTERNS.put(TIMESTAMP_FORMAT_NO_SEPARATE_MASK, TIMESTAMP_FORMAT_NO_SEPARATE);
        PATTERNS.put(TIME_FORMAT_MASK, TIME_FORMAT);
    }

    /**
     * 获取传入字符串之后time分钟的时间字符串
     *
     * @param nowTime 传入时间
     * @param time    传入间隔多少分钟
     * @return: {@link String} 返回比传入时间晚time分钟后的时间字符串：格式-yyyyMMddHHmmss
     * @author: 向传辉
     * @createTime: 2022/1/29  9:30
     */
    public static String getNowToAfterTime(String nowTime, int time) {
        if (nowTime == null) {
            return null;
        }
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat(TIMESTAMP_FORMAT_NO_SEPARATE);
        try {
            date = format.parse(nowTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date == null) {
            return null;
        }
        long t = time * 60 * 1000;
        Date afterTime = new Date(date.getTime() + t);
        return format.format(afterTime);
    }

    /**
     * 自动获取匹配的日期掩码
     *
     * @param date-日期字符串
     * @return: {@link String} 日期掩码
     * @author: 向传辉
     * @createTime: 2022/1/29  9:37
     */
    public static String getPattern(String date) {
        if (StringUtils.isBlank(date)) {
            throw new NullPointerException("input date string is null");
        }
        Set<String> patterns = PATTERNS.keySet();
        for (String pattern : patterns) {
            if (date.matches(pattern)) {
                return PATTERNS.get(pattern);
            }
        }
        throw new IllegalArgumentException("input date string is illegal argument");
    }

    /**
     * 获取当前日期，默认格式为：yyyyMMdd
     *
     * @param
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  9:48
     */
    public static String getCurrentDate() {
        return getCurrentDate(DATE_FORMAT_STRING);
    }

    /**
     * 获取当前日期 - 按指定格式
     *
     * @param template 日期格式
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  9:40
     */
    public static String getCurrentDate(String template) {
        SimpleDateFormat format = new SimpleDateFormat(template);
        return format.format(new Date());
    }

    /**
     * 获取当前时间 - 默认：HHmmss
     *
     * @param
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  9:50
     */
    public static String getCurrentTime() {
        return getCurrentTime(DATE_FORMAT_TIME);
    }

    /**
     * 获取当前时间 - 按指定格式
     *
     * @param template-日期格式
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  9:49
     */
    public static String getCurrentTime(String template) {
        SimpleDateFormat format = new SimpleDateFormat(template);
        return format.format(new Date());
    }

    /**
     * 获取当前时间 - yyyy-MM-dd HH:mm:ss格式
     *
     * @param
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  10:50
     */
    public static String getCurrentFullTime() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING_SEPARATE + " " + TIME_FORMAT);
        return format.format(new Date());
    }

    /**
     * 获取当前时间戳
     *
     * @param
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  10:59
     */
    public static String getTimestamp() {
        return getTimestamp(null);
    }

    /**
     * 获取指定时间的时间戳：格式yyyyMMddHHmmss
     *
     * @param date
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  11:01
     */
    public static String getTimestamp(final Date date) {
        SimpleDateFormat format = new SimpleDateFormat(TIMESTAMP_FORMAT_NO_SEPARATE);
        if (date != null)
            return format.format(date);
        return format.format(new Date());
    }

    /**
     * 将date类型转换为固定格式的字符串，格式默认：yyyy-MM-dd
     *
     * @param date-上送的date类型的时间
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  11:07
     */
    public static String toDateString(Date date) {
        return toDateString(date, DATE_FORMAT_STRING_SEPARATE);
    }

    /**
     * 将date类型转换为固定格式的字符串
     *
     * @param date-上送的date类型时间
     * @param format-传入的日期格式
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  11:09
     */
    public static String toDateString(Date date, String format) {
        if (date == null) {
            return null;
        }
        String dateString = null;
        try {
            SimpleDateFormat formatString = new SimpleDateFormat(format);
            dateString = formatString.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     * 将日期字符串转换为日期对象
     *
     * @param str-传入的日期字符串
     * @return: {@link Date}
     * @author: 向传辉
     * @createTime: 2022/1/29  11:22
     */
    public static Date toDate(String str) {
        String pattern = getPattern(str);
        if (TIME_FORMAT.equals(pattern) || DATE_FORMAT_TIME.equals(pattern)) {
            throw new IllegalArgumentException("Illegal date[" + str + "]");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            return dateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Illegal date[" + str + "]");
        }
    }

    /**
     * 计算输入日期n天之后，返回的是date类型
     *
     * @param date-输入的日期
     * @param n-天数
     * @return: {@link Date}-返回n天后的日期
     * @author: 向传辉
     * @createTime: 2022/1/29  11:27
     */
    public static Date nextDay(Date date, int n) {
        long dateL = date.getTime();
        // 当前日期的毫秒数加上增加n天的毫秒数
        long n_sec = 24 * 60 * 60 * 1000;
        long n_dateL = dateL + n_sec;
        return new Date(n_dateL);
    }

    /**
     * 计算输入n天后，返回的是String类型
     *
     * @param date-输入的日期
     * @param n-天数
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/1/29  14:42
     */
    public static String nextDay(String date, int n) {
        long time = toDate(date).getTime();
        // 当前日期的毫秒数加上增加n天的毫秒数
        long n_sec = 24 * 60 * 60 * 1000;
        long n_dateL = time + n_sec;
        return toDateString(new Date(n_dateL));
    }

    /**
     * 判断平闰年
     *
     * @param year - 输入年份
     * @return: {@link boolean} - 返回是闰年还是平年
     * @author: 向传辉
     * @createTime: 2022/1/29  14:46
     */
    public static boolean isLeapYear(int year) {
        if (year % 4 == 0 || (year % 100 != 0 && year % 400 == 0)) {
            return true;
        }
        return false;
    }

    /**
     * 取两个日期的时间差
     *
     * @param startDate - 起始日期
     * @param endDate   - 结束日期
     * @return: {@link int} - 结果包含起始日期和终点日期
     * @author: 向传辉
     * @createTime: 2022/1/29  14:52
     */
    public static int getDaysBetween(String startDate, String endDate) throws IllegalArgumentException {
        if (null == startDate || startDate.length() != 8) {
            throw new IllegalArgumentException("起始日期数据不正确：" + startDate);
        }
        if (null == endDate || endDate.length() != 8) {
            throw new IllegalArgumentException("结束日期数据不正确：" + endDate);
        }
        Date startD = toDate(startDate);
        Date endD = toDate(endDate);
        if (startD.after(endD)) {
            // 如果起始日期大于结束日期，则递归调用反向然后乘以复数
            // return getDaysBetween(endDate,startDate)+1*(-1);
            throw new IllegalArgumentException("起始日期大于结束日期：" + startDate + " " + endDate);
        }
        // 利用日历类进行时间的计算
        int Year = Integer.parseInt(startDate.substring(0, 4));
        int Month = Integer.parseInt(startDate.substring(4, 6)) - 1;
        int Day = Integer.parseInt(startDate.substring(6, 8));
        GregorianCalendar start = new GregorianCalendar(Year, Month, Day);

        Year = Integer.parseInt(endDate.substring(0, 4));
        Month = Integer.parseInt(endDate.substring(4, 6)) - 1;
        Day = Integer.parseInt(endDate.substring(6, 8));
        GregorianCalendar end = new GregorianCalendar(Year, Month, Day);

        int startYear = start.get(Calendar.YEAR);
        int endYear = end.get(Calendar.YEAR);

        int startDayOfYear = start.get(Calendar.DAY_OF_YEAR);
        int endDayOfYear = end.get(Calendar.DAY_OF_YEAR);

        int days = 0;

        for (int i = startYear; i < endYear; i++) {
            if (isLeapYear(i)) { // 判断平年闰年做天数的增加
                days += 366;
            } else {
                days += 365;
            }
        }
        days += (endDayOfYear - startDayOfYear + 1);
        return days;
    }

    /**
     * 校验日期是否是正确的日期格式
     *
     * @param date 格式为：20220209 或 2022-02-09 或 2022/02/09 或 2022.02.09
     * @return: {@link boolean}
     * @author: 向传辉
     * @createTime: 2022/2/9  14:21
     */
    public static boolean isLegalDateFormat(final String date) {
        String strDate = date;
        // 常用日期分割符
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("-")
                .append(".")
                .append("/");
        if (strDate == null) {
            return false;
        }
        // 日期字符串长度必须为8 或 10
        int len = strDate.trim().length();
        if (len != 8 && len != 10) {
            return false;
        }
        // 长度为10时必须有两个连接符
        if (len == 10) {
            if (strDate.charAt(4) == strDate.charAt(7) && stringBuilder.indexOf(strDate.substring(4, 5)) != -1) {
                strDate = strDate.replace(strDate.substring(4, 5), "");
            } else {
                return false;
            }
        }
        // 必须全为数字
        if (!strDate.matches("[0-9]{0,8}") || strDate.length() != 8) {
            return false;
        }

        try {
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING);
            Date dDate = format.parse(strDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dDate);
            String nDate = format.format(cal.getTime());
            return strDate.equals(StringUtils.replace(nDate, "-", ""));
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 根据标志进行日期进行计算
     *
     * @param date 要运算的日期
     * @param flag 加减标识：true-加，false-减
     * @param i    加减天数
     * @return: {@link String}
     * @author: 向传辉
     * @createTime: 2022/2/9  15:14
     */
    public static String dateOperation(String date, Boolean flag, int i) throws Exception {
        if (!isLegalDateFormat(date)) { // 判断输入得日期格式是否正确
            throw new RuntimeException("输入的日期格式不正确");
        }

        String dDate = null;
        if (date.length() == 10) { // 如果日期是带有连接符的需要去掉连接符
            dDate = date.replace(date.substring(4, 5), "");
        } else {
            dDate = date;
        }
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(dDate));
        } catch (ParseException e) {
            throw new Exception(e);
        }
        // flag是加减标志：true-加，false-减
        // i 为加减的时间（天）
        if (flag) {
            cal.set(Calendar.DATE, cal.get(Calendar.DATE) + i);
        } else {
            cal.set(Calendar.DATE, cal.get(Calendar.DATE) - i);
        }
        return format.format(cal.getTime());
    }


}
