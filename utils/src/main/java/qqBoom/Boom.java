package qqBoom;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/**
 * @author: 向传辉
 * @createTime: 2022/5/29 14:47
 */
public class Boom {
    public static void main(String[] args) throws AWTException {
        Boom boom = new Boom();
        boom.send("冤大头");
    }

    private void send(String message) throws AWTException {
        Robot robot = new Robot();
        StringSelection msg = new StringSelection(message);
        //System是系统版的粘贴板
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        robot.delay(3000);
        systemClipboard.setContents(msg, null);
        for (int i = 0; i < 5; i++) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.delay(500);
        }

    }

}
