package com.tool.utils;

import com.tool.model.Person1;
import com.tool.model.Person2;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@SpringBootTest
class UtilsApplicationTests {

    Logger logger = LoggerFactory.getLogger(UtilsApplicationTests.class);

    @Test
    void contextLoads() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Person1 person1 = new Person1("张三", 18, format.format(new Date()));
        Person2 person2 = new Person2();
        System.out.println("person1 = " + person1);
        System.out.println("person2 = " + person2);
        BeanUtils.copyProperties1(person1, person2);
        System.out.println("person1 = " + person1);
        System.out.println("person2 = " + person2);
    }

    @Test
    void contextTest1() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Person1 person1 = new Person1("张三", 18, format.format(new Date()));
        String strXml = BeanConverter.convertToXml(person1, "UTF-8");
        System.out.println(strXml);
    }

    @Test
    void contextTest2() {
        String strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<person2>\n" +
                "    <age>18</age>\n" +
                "    <birthDay>2022-01-27</birthDay>\n" +
                "    <name>张三</name>\n" +
                "</person2>";
        Person2 person2 = BeanConverter.convertToJavaBean(strXml, Person2.class);
        logger.info("将xml文档转换后得java对象：{}", person2);
    }

    @Test
    void test2() {
        String legalDateFormat = null;
        try {
            legalDateFormat = DateUtil.dateOperation("2022-03-06", false, 1);
        } catch (Exception e) {
            logger.info(e.getMessage());
//            e.printStackTrace();
        }
        logger.info("结果：{}", legalDateFormat);
    }

    @Test
    void test3() {
        Person1 p = new Person1("张三", 18, "2022-03-10");

        logger.info("map : {}", BeanUtils.beanToMap(p));
    }
}
