package com.tool.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class BeanUtils {
    private static final Logger logger = LoggerFactory.getLogger(BeanUtils.class);

    /**
     * 对象参数复制：将source中的参数赋值给target
     *
     * @param source 被复制的对象
     * @param target 接收对象
     * @throws BeansException
     */
    public static void copyProperties1(Object source, Object target) throws BeansException {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "target must not be null");

        Class<?> targetObj = target.getClass();
        PropertyDescriptor[] targetDescriptors = org.springframework.beans.BeanUtils.getPropertyDescriptors(targetObj);

        for (PropertyDescriptor targetDescriptor : targetDescriptors) {
            Method writeMethod = targetDescriptor.getWriteMethod();
            if (writeMethod != null) {
                PropertyDescriptor sourceDescriptor = org.springframework.beans.BeanUtils.getPropertyDescriptor(source.getClass(), targetDescriptor.getName());
                if (sourceDescriptor != null) {
                    Method readMethod = sourceDescriptor.getReadMethod();
                    if (readMethod != null &&
                            ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);

                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            if (value != null) {
                                writeMethod.invoke(target, value);
                            }
                        } catch (Throwable e) {
                            System.out.println(e.getMessage());
                            throw new FatalBeanException("Failed to copy" + targetDescriptor.getName() + " to " + sourceDescriptor.getName());
                        }
                    }
                }
            }
        }
    }

    /**
     * 将javaBean转换成map对象
     *
     * @param obj javaBean对象
     * @return: {@link Map< String, Object>}
     * @author: 向传辉
     * @createTime: 2022/2/10  16:11
     */
    public static Map<String, Object> beanToMap(Object obj) {
        if (obj == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            for (PropertyDescriptor property : beanInfo.getPropertyDescriptors()) {
                String key = property.getName();
                if (!key.equals("class")) {
                    Method method = property.getReadMethod();
                    Object value = method.invoke(obj);
                    System.out.println("key : " + key + "\tvalue : " + value);
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return map;
    }

    /**
     * 将map对象转换成javaBean对象
     *
     * @param map 传入的map对象
     * @param obj 需要赋值的java对象
     * @return: {@link Object}
     * @author: 向传辉
     * @createTime: 2022/2/10  16:22
     */
    public static Object mapToBean(Map<String, Object> map, Object obj) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());           // 1.获取对象信息
            for (PropertyDescriptor property : beanInfo.getPropertyDescriptors()) { // 2.遍历对象属性
                String key = property.getName();                                    // 3.获取属性名称
                if (map.containsKey(key)) {                                         // 4.判断该属性名称在map中是否有键对应
                    Method method = property.getWriteMethod();                      // 5.获取属性的setter方法
                    Object value = map.get(key);                                    // 6.获取map中对应键值对的值
                    method.invoke(obj, value);                                      // 7.给对象属性赋值
                }
            }
        } catch (Exception e) {
            logger.info("对象转换错误：" + e.getMessage());                        // 8.执行报错，抛出异常（不会执行第9步，直接结束）
        }
        return obj;                                                                // 9.返回赋值后的对象
    }
}
